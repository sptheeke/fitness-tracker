var express = require('express');
var router = express.Router();

/* GET login page. */
router.get('/', function(req, res, next) {
  res.render('loginPage');
});

/*
 * GET userlist.
 */
router.get('/userlist', function(req, res) {// /userlist is the path for the JQuery GET call
    var db = req.db;

    var collection = db.get('userlist');//specifies what data to retrieve from db
    collection.find({},{},function(e,docs){
        res.json(docs);
    });
});

module.exports = router;