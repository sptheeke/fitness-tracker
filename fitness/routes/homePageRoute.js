var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	var user = req.body.user;

  	res.render('homePage', {user: user});
});

module.exports = router;