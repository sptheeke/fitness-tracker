$(document).ready(function() {
	buildPieChart();
	buildBarChart();
	buildLineChart();
	document.getElementById("icons").hidden = false;
	$('#addWorkoutIcon').on('click', function(event){
		document.getElementById("addWorkoutForm").hidden = false;
		document.getElementById("shadow").hidden = false;
	});
	$('#addMealIcon').on('click', function(event){
		document.getElementById("addMealForm").hidden = false;
		document.getElementById("shadow").hidden = false;
	});
	$('#addWorkoutButton').on('click', addWorkout);
	$('#addMealButton').on('click', addMeal);
	$('#shadow').on('click', closeModals);
	$('#lineChartBlock').on('click', closeModals);
	$('#barChartBlock').on('click', closeModals);
	$('#pieChartBlock').on('click', closeModals);
	$('#logoutIcon').on('click', logout);
});

function closeModals(event){
	document.getElementById("addWorkoutForm").hidden = true;
	document.getElementById("addMealForm").hidden = true;
	document.getElementById("shadow").hidden = true;
}

function logout(event){
	window.location.href='/';
}

function addWorkout(event){
	closeModals();
}

function addMeal(event){
	closeModals();
}

function buildPieChart(){
	var chart = c3.generate({
	    bindto: '#pieChart',
	    data: {
	      columns: [
	        ['Bench', 30],
	        ['Squat', 50],
	        ['Row', 24],
	        ['Deadlift', 35],
	        ['Curls', 12]
	      ],
	      type: 'pie'
	    }
	});
}

function buildBarChart(){
	var chart = c3.generate({
	    bindto: '#barChart',
	    data: {
	      columns: [
	        ['Bench', 130],
	        ['Squat', 150],
	        ['Curls', 25],
	        ['Deadlift', 200]
	      ],
	      type: 'bar'
	    },
	    axis: {
        	x: {
            	label: {
            		text: 'Exercise',
            		position: 'outer-center'
               	 	// inner-right : default
                	// inner-center
                	// inner-left
                	// outer-right
                	// outer-center
                	// outer-left
            	},
            	tick:{
            		values: [""]
            	}
        	},
        	y: {
            	label: {
            		text:'Weight (lbs)',
                	position: 'outer-middle'
                	// inner-top : default
                	// inner-middle
                	// inner-bottom
                	// outer-top
                	// outer-middle
                	// outer-bottom
                }
        	}
    	}
	});
}

function buildLineChart(){
	var chart = c3.generate({
		bindto: '#lineChart',
	    data: {
	    	x: 'Date',
	        columns: [
	        	['Date', '2013-01-05', '2013-01-10', '2013-01-15', '2013-01-19', '2013-01-20', '2013-01-23'],
	            ['Weight', 205, 200, 200, 201, 202, 203],
	        ]
	    },
	    legend: {
        	show: false
    	},
	    axis: {
        	x: {
        		type: 'timeseries',
            	label: {
            		text: 'Date',
            		position: 'outer-center'
               	 	// inner-right : default
                	// inner-center
                	// inner-left
                	// outer-right
                	// outer-center
                	// outer-left
            	},
        	},
        	y: {
            	label: {
            		text:'Weight (lbs)',
                	position: 'outer-middle'
                	// inner-top : default
                	// inner-middle
                	// inner-bottom
                	// outer-top
                	// outer-middle
                	// outer-bottom
                }
        	}
    	}
	});
}