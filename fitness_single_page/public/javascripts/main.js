/******** Global Variables ********/
var user = null;
var exercisesShown = 1;
var foodShown = 1;

/******** App Ready Function ********/
$(document).ready(function() {
	$('#loginButton').on('click', validateUser);
	$('#forgotPassword').on('click', showForgotPasswordModal);
	$('#register').on('click', showRegistrationModal);
	$('#shadow').on('click', closeModals);
	$('#sendResetPasswordEmail').on('click', resetPassword);
	$('#registerNewUserButton').on('click', registerNewUser);
});

/******** Login Page Functions ********/
function validateUser(event){

	var username = document.getElementById("username").value;
	var password = document.getElementById("password").value;

	$.getJSON( '/userlist', function(users) {
		console.log(users);
		for (var i = 0; i < users.length; i ++){
			if (users[i].username == username && users[i].password == password){
				user = users[i];
				loadMainPage();
			}
		}
		document.getElementById("userNotAuthenticated").hidden = false;
    });
}

function resetPassword(event){
	// validate fields
	var email = document.getElementById("forgotPasswordEmailInput").value;

	var userToReset;
	$.getJSON( '/userlist', function(users) {
		for (var i = 0; i < users.length; i ++){
			if (users[i].email == email){
				userToReset = users[i];
			}
		}
		if (userToReset != null){
			$.ajax({
		        type: 'PUT',
		        data: userToReset,
		        url: '/resetpassword',
		        dataType: 'JSON'
		    }).done(function( response ) {
		    		// account for error response
		        	closeModals();
		    });
		} 
		else{
			console.log("User does not exist");
		}
    });
}

function registerNewUser(event){
	var email = document.getElementById("newUserEmail").value;
	var password = document.getElementById("newUserPassword").value;
	var confirmPassord = document.getElementById("newUserConfirmPassword").value;

	var validated = true;

	if (email.length == 0 || password.length == 0 || confirmPassord.length == 0){
		validated = false;
		console.log("Empty Fields");
	}

	if (password != confirmPassord){
		validated = false;
		console.log("Passwords don't match");
	}

	var username = buildUsername(email);

	if (validated){

	    var newUser = {
	        'username': username,
	        'email': email,
	        'password': password,
	        'workouts': [],
	        'meals': [],
	        'weighIns': []
	    }

	    $.ajax({
	        type: 'POST',
	        data: newUser,
	        url: '/adduser',
	        dataType: 'JSON'
	    }).done(function( response ) {
	    	// send confirmation email?
	    	// analyze response for error
	    	closeModals();
	    });
	}
}

function buildUsername(email){
	var username = email.split('@')[0];
	//need more validation
	console.log(username);
	return username;
}

/******** Main Page Functions ********/
function loadMainPage(){
	document.getElementById("loginPage").hidden = true;
	document.getElementById("homePage").hidden = false;
	buildPieChart();
	buildBarChart();
	buildLineChart();
	document.getElementById("icons").hidden = false;
	$('#addWorkoutIcon').on('click', showAddWorkoutModal);
	$('#addMealIcon').on('click', showAddMealModal);
	$('#addWorkoutButton').on('click', addWorkout);
	$('#addMealButton').on('click', addMeal);
	$('#addExerciseButton').on('click', addExercise);
	$('#addFoodButton').on('click', addFood);
	$('#shadow').on('click', closeModals);
	$('#lineChartBlock').on('click', closeModals);
	$('#barChartBlock').on('click', closeModals);
	$('#pieChartBlock').on('click', closeModals);
	$('#logoutIcon').on('click', logout);
}

function logout(event){
	document.getElementById("icons").hidden = true;
	document.getElementById("loginPage").hidden = false;
	document.getElementById("homePage").hidden = true;
	document.getElementById("userNotAuthenticated").hidden = true;
	user = null;
}

function addMeal(event){
	// Get input field values
	// validate fields
	// create json workout object
	// PUT new workout into user array
	closeModals();
}

function addExercise(event){
	exercisesShown++;
	document.getElementById("exerciseForm" + exercisesShown).hidden = false;
}

function addWorkout(event){
	var newWorkout = [];
	for (var i = 1; i < exercisesShown+1; i++){
		// Get input field values
		// validate fields
		var exercise = document.getElementById("exerciseName" + i).value;
		var numSets = document.getElementById("numSets" + i).value;
		var numReps = document.getElementById("numReps" + i).value;
		var weight = document.getElementById("weight" + i).value;
		// create json meal object
		newWorkout.push({
			"exercisename": exercise,
			"sets": numSets,
			"reps": numReps,
			"weight": weight,
			"primarymusclegroup": "Chest",
			"secondarymusclegroup": "Arms"
		});
	}
	// var workout = { "workout" : newWorkout };

	// Need to pass workout as parameter or query or something.................... 
	$.ajax({
		type: 'PUT',
		data: newWorkout,
		url: '/addWorkout/' + user._id,
		dataType: 'JSON'
	}).done(function( response ) {
		// account for error response
		closeModals();
	});

	console.log(newWorkout);
	// PUT new meal into user array
	closeModals();
}

function addFood(event){
	foodShown++;
	document.getElementById("foodForm" + foodShown).hidden = false;
}

function buildPieChart(){
	var chart = c3.generate({
	    bindto: '#pieChart',
	    data: {
	      columns: [
	        ['Bench', 30],
	        ['Squat', 50],
	        ['Row', 24],
	        ['Deadlift', 35],
	        ['Curls', 12]
	      ],
	      type: 'pie'
	    }
	});
}

function buildBarChart(){
	var chart = c3.generate({
	    bindto: '#barChart',
	    data: {
	      columns: [
	        ['Bench', 130],
	        ['Squat', 150],
	        ['Curls', 25],
	        ['Deadlift', 200]
	      ],
	      type: 'bar'
	    },
	    axis: {
        	x: {
            	label: {
            		text: 'Exercise',
            		position: 'outer-center'
            	},
            	tick:{
            		values: [""]
            	}
        	},
        	y: {
            	label: {
            		text:'Weight (lbs)',
                	position: 'outer-middle'
                }
        	}
    	}
	});
}

function buildLineChart(){
	var chart = c3.generate({
		bindto: '#lineChart',
	    data: {
	    	x: 'Date',
	        columns: [
	        	['Date', '2013-01-05', '2013-01-10', '2013-01-15', '2013-01-19', '2013-01-20', '2013-01-23'],
	            ['Weight', 205, 200, 200, 201, 202, 203],
	        ]
	    },
	    legend: {
        	show: false
    	},
	    axis: {
        	x: {
        		type: 'timeseries',
            	label: {
            		text: 'Date',
            		position: 'outer-center'
            	},
        	},
        	y: {
            	label: {
            		text:'Weight (lbs)',
                	position: 'outer-middle'
                }
        	}
    	}
	});
}

/******** Modal/Element Functions ********/
function showForgotPasswordModal(event){
	document.getElementById("forgotPasswordForm").hidden = false;
	document.getElementById("shadow").hidden = false;
}

function showRegistrationModal(event){
	document.getElementById("registerNewUserForm").hidden = false;
	document.getElementById("shadow").hidden = false;
}

function showAddWorkoutModal(event){
	document.getElementById("addWorkoutForm").hidden = false;
	document.getElementById("shadow").hidden = false;
}

function showAddMealModal(event){
	document.getElementById("addMealForm").hidden = false;
	document.getElementById("shadow").hidden = false;	
}

function emptyInputFields(){
	document.getElementById("username").value = '';
	document.getElementById("password").value = '';
	document.getElementById("forgotPasswordEmailInput").value = '';
	document.getElementById("newUserEmail").value = '';
	document.getElementById("newUserPassword").value = '';
	document.getElementById("newUserConfirmPassword").value = '';
}

function hideAllLinesForModals(){
	for (var i = 2; i < 8; i++){
		document.getElementById("exerciseForm" + i).hidden = true;
		document.getElementById("foodForm" + i).hidden = true;
	}
}

function closeModals(event){
	foodShown = 1;
	exercisesShown = 1;
	emptyInputFields();
	document.getElementById("forgotPasswordForm").hidden = true;
	document.getElementById("registerNewUserForm").hidden = true;
	document.getElementById("addWorkoutForm").hidden = true;
	document.getElementById("addMealForm").hidden = true;
	document.getElementById("shadow").hidden = true;
}