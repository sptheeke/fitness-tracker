var express = require('express');
var router = express.Router();

/* GET login page. */
router.get('/', function(req, res, next) {
  res.render('main');
});

/*
 * GET userlist.
 */
router.get('/userlist', function(req, res) {// /userlist is the path for the JQuery GET call
    var db = req.db;

    var collection = db.get('userlist');//specifies what data to retrieve from db
    collection.find({},{},function(e,docs){
        res.json(docs);
    });
});

/*
 * POST to adduser.
 */
router.post('/adduser', function(req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    collection.insert(req.body, function(err, result){
        res.send(
            (err === null) ? { msg: '' } : { msg: err }
        );
    });
});

/*
 * PUT to resetpassword.
 */
router.put('/resetpassword', function(req, res) {
	var db = req.db;
    var collection = db.get('userlist');
    var user = req.body;

    // make the new password something random instead of yo
	collection.findOneAndUpdate({"_id": user._id}, {$set: {password: "yo"}}, {sort: {_id: -1}}, 
		(err, result) => {
			// account for error
			res.send(result);
  	});
});

/*
 * PUT to resetpassword.
 */
router.put('/addWorkout/:userId', function(req, res) {
	var db = req.db;
    var collection = db.get('userlist');
    var userId = req.param("userId"); // Correct Id - this works
    var workout = req.body; //Still Struggling reading this as an array

    console.log(workout);


 //    // make the new password something random instead of yo
	// collection.findOneAndUpdate({"_id": user._id}, {$set: {workouts: "yo"}}, {sort: {_id: -1}}, 
	// 	(err, result) => {
	// 		// account for error
	// 		res.send(result);
 //  	});
});

module.exports = router;